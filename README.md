![设备管理](app-equip-dev/icon-50.png)
# 设备管理移动端
## 产品版下载
* [Android下载](https://gitee.com/dyflyleaf/testiosdeploy/raw/master/app-equip-product/android-release-product.apk)
* IOS版本下载
    * 拷贝如下地址在苹果手机safari浏览器打开
    
     itms-services:///?action=download-manifest&amp;url=https://gitee.com/dyflyleaf/testiosdeploy/raw/master/app-equip-product/app-equip-product.plist
    * 出现如下图片
    * 点击是
    * 点击安装
    * “设备管理APP”下载安装页面已经在下载
    * 下载完成启动APP
    
## 开发版下载
* [Android下载](https://gitee.com/dyflyleaf/testiosdeploy/raw/master/app-equip-dev/android-release-dev.apk)
    * 拷贝如下地址在苹果手机safari浏览器打开
    
     itms-services:///?action=download-manifest&amp;url=https://gitee.com/dyflyleaf/testiosdeploy/raw/master/app-equip-dev/app-equip-dev.plist
    * 出现如下图片
    * 点击是
    * 点击安装
    * 回到手机桌面,“设备管理APP”下载安装页面已经在下载
    * 下载完成启动APP


##有问题反馈
在使用中有任何问题，欢迎反馈给我，可以用以下联系方式跟我交流

* 邮件(dongyong@inspur.com)
* QQ: 278813035
